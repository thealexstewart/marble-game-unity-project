using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerCamera : MonoBehaviour
{
    // Variables
    // Player position data
    [SerializeField]
    private DataPlayerTransform _playerTransform;
    // Time it takes when smoothing
    public float smoothTime;
    // Offset for the camera, to position it away from the player
    public Vector3 CamOffset;
    // How far the camera can go before it stops moving in a direction
    [SerializeField]
    private DataPlayerCameraLimits _cameraLimits;
    // The current camera position, to alter
    private Vector3 _cameraPosition;
    // How fast the camera is moving when smoothing
    private Vector3 _velocity = Vector3.zero;
    
    // UnityEvent for when the y-axis needs to reset
    public UnityEvent YAxisReset;
    
    void FixedUpdate() 
    {
        _cameraPosition = new Vector3(_playerTransform.PlayerPosition.x + CamOffset.x, _playerTransform.PlayerPosition.y + CamOffset.y, _playerTransform.PlayerPosition.z + CamOffset.z);
        _cameraPosition.x = Mathf.Clamp(_cameraPosition.x, _cameraLimits.CamLimitX.x, _cameraLimits.CamLimitX.y);
        _cameraPosition.y = Mathf.Clamp(_cameraPosition.y, _cameraLimits.CamLimitY.x, _cameraLimits.CamLimitY.y);
        transform.position = Vector3.SmoothDamp(transform.position, _cameraPosition, ref _velocity, smoothTime);
    }
    
    public void CameraResetYAxis()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + 10.0f, transform.position.z);
    }
    
}
