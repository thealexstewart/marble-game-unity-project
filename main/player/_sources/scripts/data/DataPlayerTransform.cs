using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/PlayerTransform", fileName = "New Player Transform")]
public class DataPlayerTransform : ScriptableObject
{
    // Variables
    // Player position, stored
    public Vector3 PlayerPosition;
    // Respawn position for the level
    public Vector3 PlayerRestartPosition;
}
