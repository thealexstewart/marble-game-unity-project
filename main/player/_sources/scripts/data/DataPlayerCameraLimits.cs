using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/PlayerCameraLimits", fileName = "New Player Camera Limits")]
public class DataPlayerCameraLimits : ScriptableObject
{
    public Vector2 CamLimitX;
    public Vector2 CamLimitY;
}
