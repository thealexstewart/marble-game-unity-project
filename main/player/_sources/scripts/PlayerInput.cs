using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Controlnput;

public class PlayerInput : MonoBehaviour, GameplayControls.IGameplayActions
{
    // Fields and Properties
    // Input Vector
    protected Vector2 _inputVector;
    
    // Methods
     // Called when the player performs the Tilt action
    public virtual void OnTilt(InputAction.CallbackContext context)
    {
        // Read the input value
        _inputVector = context.ReadValue<Vector2>();
    }
    
    // Called when the player performs the Pause action
    public virtual void OnPause(InputAction.CallbackContext context)
    {
        Debug.Log("Pause");
    }
}
