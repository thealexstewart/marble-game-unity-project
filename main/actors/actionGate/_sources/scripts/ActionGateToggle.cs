using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ActionGateToggle : Actor
{
    // Variables
    // Where does the gate start?
    private Vector3 _startPosition;
    // Where should the gate go, when the button is pressed?
    [SerializeField]
    private Vector3 _targetPosition;
    // Time passed
    [Range(0,1)]
    private float _timePassed;
    // Gate Speed
    [SerializeField]
    private float _speed;

    // Functions
    // Called when object starts. Overrides the base function
    protected override void Start()
    {
        base.Start();
        _startPosition = transform.localPosition;
    }
    // Called every physics frame
    void FixedUpdate()
    {
        // Add time
        _timePassed += _speed;
        // Is the gate open? Determine where the gate should move towards
        if(IsActorActive)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, _targetPosition, _timePassed);
        }
        else
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, _startPosition, _timePassed);
        }
    }
    // When the state is changed
    public override void OnStateChanged()
    {
        // Reset the timer, then call the base function
        _timePassed = 0.0f;
        base.OnStateChanged();
    }
}
