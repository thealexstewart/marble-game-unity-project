using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidingActors : Actor
{
    // Fields / Properties
    // The Mesh renderer
    private Renderer _renderer;
    
    // Methods
    // Called on start
    protected override void Start()
    {
        base.Start();
        // Set our Renderer
        _renderer = GetComponent<Renderer>();
        CheckRendererState();
    }
    // Called when the state is changed
    public override void OnStateChanged()
    {
        //Invert our object's state
        _renderer.enabled = !_renderer.enabled;
        base.OnStateChanged();
    }
    // Called when the player respawns
    public override void OnPlayerRespawn()
    {
        base.OnPlayerRespawn();
        // Set the renderer state back to normal
        CheckRendererState();
    }
    // A check to see if the renderer should be active or not
    void CheckRendererState()
    {
        if(IsActorActive)
        {
            _renderer.enabled = true;
        }
        else
        {
            _renderer.enabled = false;
        }
    }
}
