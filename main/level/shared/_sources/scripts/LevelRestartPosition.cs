using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelRestartPosition : MonoBehaviour
{
    // Variables
    // Player Transform
    [SerializeField]
    private DataPlayerTransform _playerTransform;
    
    // Functions
    // Called every physics update
    void FixedUpdate()
    {
        // Set the player's respawn position to this position
        _playerTransform.PlayerRestartPosition = transform.position;
    }
}
