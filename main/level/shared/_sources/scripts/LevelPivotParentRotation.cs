using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Controlnput;

public class LevelPivotParentRotation : PlayerInput
{
    // Variables
    // Player Transform
    [SerializeField]
    private DataPlayerTransform _playerTransform;
    // Y-axis Reset Event
    [SerializeField]
    private DataEvent _yAxisReset;
    // Tilt Speed
    [SerializeField]
    private float _maxAngle;
    // Smoothness Damping 
    [SerializeField]
    private float _smoothDamp;
    // Time passed
    private float _timePassed;
    // Gameplay Controls, for adding callbacks
    private GameplayControls _controls;
    // Quaternion to store our input rotation
    private Quaternion _targetInput;
    
    // Arrays for child objects, so we can update their locations/rotations properly
    private Transform[] _children;
    private Vector3[] _childrenPositions;
    private Quaternion[] _childrenRotations;
    
    // Called when the script is enabled
    void OnEnable()
    {
        if (_controls == null)
        {
            // Add our control input, and add callbacks for it
            _controls = new GameplayControls();
            _controls.Gameplay.SetCallbacks(this);
        }
        // Enable the control input
        _controls.Gameplay.Enable();
        
        // Size our array, to store all child objects of the level pivot
        _children = new Transform[transform.childCount];
        // Iterate through this new array size, and set it to every child object in our level
        for(int i = _children.Length - 1; i >= 0; i--)
		{
            _children[i] = transform.GetChild(i);
        }
        // Size the other arrays, where we will store their positions and rotations
        _childrenPositions = new Vector3[_children.Length];
        _childrenRotations = new Quaternion[_children.Length];
    }
    
    // Called every phhusics frame
    void FixedUpdate()
    {
        // If we are inputting into a certain value, set our Vector2. Otherwise, make it 0
        if(_inputVector != Vector2.zero)
        {
            // Rotate based on our input times our tilt speed
            _targetInput = Quaternion.Euler(_inputVector.y * _maxAngle, 0, -_inputVector.x * _maxAngle);
        }
        else
        {
            _targetInput = Quaternion.Euler(0,0,0);
        }
        // Add to our time passed float, for when we want smooth tilts
        if(_timePassed <= 1.0f)
        {
            _timePassed += 0.1f;
        }
        // Apply the rotation smoothly
        transform.rotation = Quaternion.Slerp(transform.rotation, _targetInput, _timePassed);
        // Update the parent pivot
        SetParentPivot();
        // Check if the position is below -10f. If so, we need to push it up, and tell the player to push it up, by calling an event
        if(transform.position.y < -10.0f)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 10.0f, transform.position.z);
            _yAxisReset.Trigger();
        }
    }
    
    // Called when the player performs the Tilt action
    public override void OnTilt(InputAction.CallbackContext context)
    {
        // Read the input value
        base.OnTilt(context);
        // Since we input a new value, we want to reset the time passed float, so smooth movement will restart
        _timePassed = 0;
    }

    // The most complex code here, repurposed from a plugin (thanks @yasirkula! https://github.com/yasirkula/UnityAdjustPivot) 
    // Function to set the pivot point to where the player is
    private void SetParentPivot()
	{
        // First, iterate through each child object
		for(int i = _children.Length - 1; i >= 0; i--)
		{
            // Update the rotations and positions to our child objects rotations and positions
			_childrenPositions[i] = _children[i].position;
			_childrenRotations[i] = _children[i].rotation;
		}
		// Set the pivot parent position and rotation to the pivot position and rotation
        transform.position = _playerTransform.PlayerPosition;

        // Iterate again through each child object
		for(int i = 0; i < _children.Length; i++)
		{
            // This time, move each child object and rotation back to where it should be. This ensures the level doesn't really move
			_children[i].position = _childrenPositions[i];
			_children[i].rotation = _childrenRotations[i];
		}
	}
}
