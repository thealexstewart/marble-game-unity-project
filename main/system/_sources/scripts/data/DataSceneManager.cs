using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "ScriptableObjects/SceneManager", fileName = "New Scene Manager")]
public class DataSceneManager : ScriptableObject
{
    // Fields / Properties
    // Current scene
    private int _currentScene = 0;
    // The event to toggle
    [SerializeField]
    private DataEvent _timerEvent;
    
    // Methods
    // Called when the application needs to quit
    public void QuitApp()
    {
        Application.Quit();
    }
    // Called when an object needs to load the cameraCanvas scene and the first level
    public void StartGame()
    {
        // Load our two scenes together, then set the counter
        SceneManager.LoadSceneAsync(2);
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);
        _currentScene = 2;
    }
    // Load a new scene
    public void LoadNextScene()
    {
        // If we run out of scenes, loop back
        if(_currentScene >= SceneManager.sceneCountInBuildSettings - 1)
        {
            // Load the main menu
            _currentScene = 0;
            SceneManager.LoadSceneAsync(_currentScene);
        }
        else
        {
            // Unload the old scene, and load the new one
            SceneManager.UnloadSceneAsync(_currentScene);
            _currentScene++;
            SceneManager.LoadSceneAsync(_currentScene, LoadSceneMode.Additive);
            // Restart the timers
            _timerEvent.Trigger();
        }
    }
}
