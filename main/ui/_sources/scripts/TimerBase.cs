using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class TimerBase : MonoBehaviour
{
    // Fields / Properties
    // Is the timer paused?
    protected bool IsTimerPaused = false;
    // Current time stored, in seconds
    protected float Seconds;
    // Minutes
    protected int Minutes;
    //Timer text
    protected TextMeshProUGUI Text;
    // What is this timer called? This is only changed in inherited classes
    protected string TimerName = "Total";
    
    // UnityEvent for when the timer is toggled
    public UnityEvent TimerPause;
    
    
    // Methods
    // Called when the object is started
    protected virtual void Start()
    {
        // Add the listener
        TimerPause.AddListener(OnToggle);
        // Add our text
        Text = GetComponent<TextMeshProUGUI>();
        // Ensure the timer is not paused when game starts
        IsTimerPaused = false;
    }
    // Called every frame
    protected virtual void Update()
    {
        // Only update this if the timer is toggled on
        if(!IsTimerPaused)
        {
            // Add to time, rounded
            Seconds += Time.deltaTime;
            // Make sure minutes are counted up
            if(Seconds >= 60)
            {
                Minutes += 1;
                Seconds -= 60;
            }
            // After all that, update the timer text, using a special String format
            Text.text = string.Format("{0}: {1}:{2:#00.00}", TimerName, Minutes, Seconds);
        }
    }
    // Called when the timer it toggled (paused or unpaused)
    public virtual void OnToggle()
    {
        IsTimerPaused = !IsTimerPaused;
    }
}
