# Adding to Project
## Prerequisites
- Unity 2020.2.1f1
- Unity Hub 2.4.2 (or newer) (optional)
- Input System 1.0.1 (or newer)
- Universal Render Pipeline 10.2.2 (or newer) (can be substituted)
- TextMeshPro 3.0.1 (or newer)

## Setup
1. Create a project with desired settings.
2. Ensure the project has Universal Render Pipeline and Input System package (from the Unity registry listing in the Package Manager)(this project provides a preconfigured Controls file).
3. Add contents of this project into the `Assets` folder of the new project.
4. In Build Settings, add the following scenes, in the specific order:
   1. `mainMenu.unity`, found in `main/system/mainMenu.unity`
   2. `cameraCanvas.unity`, found in `main/system/cameraCanvas.unity`
   3. `sampleScene1.unity`, found in `main/level/sampleScene1.unity`
   4. `sampleScene2.unity`, found in `main/level/sampleScene2.unity`
   5. Any additional level scenes you want can be added after these
5. Load `mainMenu.unity` to start the game in the editor.

(Please note that there is a bug with input. Press an input down when starting a level to prevent being stuck)

## Using Events
The project uses ScriptableObjects to call events. This allows a way to toggle states from one object to another, without a direct reference. In order to set it up for new objects, there is some setup required. `Actor` prefabs show good examples of this system in action (ie. between a `Button` and a `Pipe`). However, if a new `Actor` is wanted, it is setup like this:

### Actor Triggering the Event
1. Add an `ActorTriggerEvent` component to a collision object, with `IsTrigger` enabled. 
2. Write the name of the tag you want to look for (when an object enters the collider). 
3. If you want the object to also trigger when an object leaves, toggle `ShouldLeavingTrigger`. 
4. Pass the `DataEvent` object you want to trigger.

### Data Event
When creating a new asset, in the `ScriptableObjects` category, there is a `DataEvent` object. Create this to setup an event.

### Listening For Event Triggers
1. Add the `Actor`-inherited component for whatever object you are creating (ie. `PipeForce` for pipes). 
2. Add two `DataEventListener` components to the object.
3. For one `DataEventListener`, add a `DataEvent` that will trigger the `Actor` object. You can add as many as you want.
   - Increase the UnityEvent it is listening for, and drag the `Actor`-inherited component directly into the list.
   - Set the function it needs to call.
4. For the other `DataEventListener`, repeat step 3. Only here, we are doing so for the Player Respawn `DataEvent`.

If creating your own class, inherit from the `Actor` class. Otherwise, ensure that the proper `UnityEvents` are setup as in the `Actor` class (ie. `ActorStateEvent` or `PlayerRespawnedEvent`). 
