# Marble Game - Unity Project (C#)
Marble Game is an open source implementation of _Far From The Tree_, a game made within two weeks with a group of friends. This project contains all that would be needed to make a marble styled game.

![alt text](main/ui/_sources/textures/sprites/MarbleProjectBanner1.png "Banner")

## Included in the Project:
- Tilt level around a player pivot
- Force push physics
- Spring-based button
- Flexible event system
- Toggle-based gates
- Clock w/ carried over progression
- Save & load fastest completion times via external file
- ScriptableObject data storage
- And more!

## Download Instructions
### Source Code
Everything that is contained within the `Assets` folder is here. Either download it as an archive to extract directly into your project, or clone the project directly.

## Project Setup
Unity 2020.2.1f1 was used for the project. Please see [INSTALL.md](INSTALL.md) for info on how to setup the Source Code for using in Unity.

## Controls
Using the WASD keys on a standard US keyboard tilts the level around. While the project contains limited controller support, it was not fully developed.

## Far From The Tree
Want to see what this game looks like with a fully-fleshed out level and art assets? Check it out here: [thealexstewart.itch.io](https://thealexstewart.itch.io/far-from-the-tree)

## License
This project uses the MIT License, found in [LICENSE.](LICENSE) Please note that assets from _Far From The Tree_ are not under the same open license.
